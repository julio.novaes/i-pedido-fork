using Domain.Clientes;

namespace WebApplication
{
    public class ClienteExterno
    {
        public string Id { get; set; }

        public string NumeroContribuinte { get; set; }

        public string Nome { get; set; }

        public string Cidade { get; set; }


        public Cliente ParaCliente()
        {
            return Cliente.Iniciar()
                .ComCidade(Cidade)
                .ComNumeroContribuinte(NumeroContribuinte)
                .ComNome(Nome)
                .Criar();
        }
    }
}