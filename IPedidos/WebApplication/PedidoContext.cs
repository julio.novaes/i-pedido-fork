using Domain.Vendendors;
using Microsoft.EntityFrameworkCore;

namespace WebApplication
{
    public class PedidoContext:DbContext
    {
        public PedidoContext(DbContextOptions<PedidoContext>options)
            :base(options)
        {
        }
        public DbSet<VendendorExterno> Vendendores { get; set; }
        public DbSet<ClienteExterno> Clientes { get; set; }

      


    }
}