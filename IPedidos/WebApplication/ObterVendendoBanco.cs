using System.Linq;
using Domain.Vendendors;

namespace WebApplication
{
    public class ObterVendendoBanco:IObterVendedor
    {
        private PedidoContext _context;
        public ObterVendendoBanco(PedidoContext context)
        {
            _context = context;
        }

        public Vendedor BuscarVendendor(string numeroInstitucional)
        {
            return _context.Vendendores
                .First(cliente => cliente.Nome == numeroInstitucional)
                .ParaVendedor();
        }
    }
}