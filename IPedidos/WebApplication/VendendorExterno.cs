using Domain.Vendendors;

namespace WebApplication
{
    public class VendendorExterno
    {
        
        public long Id { get; set; }
        public string Nome { get; set;}


        public Vendedor ParaVendedor()
        {
            return Vendedor.ComNome(Nome);
        }

    }
}