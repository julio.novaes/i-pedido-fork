using System.Text.Json.Serialization;
using Domain.FormaPagamentos;

namespace WebApplication
{
    public class FormaPagamentoExterna
    {
        [JsonPropertyName("Forma de Pagamento")]
        public string Descricao { get; set; }

        public FormaPagamento paraForma()
        {
            return FormaPagamento.ComDescricao(Descricao);
        }

    }
}