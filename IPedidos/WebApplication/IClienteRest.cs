#nullable enable
using System;
using System.Threading.Tasks;

namespace WebApplication
{
    public interface IClienteRest
    {
        public Task<FormaPagamentoExterna?> obterFromaExterna(String forma);

    }
}