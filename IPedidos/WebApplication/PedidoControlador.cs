using System.Threading.Tasks;
using Domain.FormaPagamentos;
using Domain.Vendendors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApplication
{
    [Route("Pedido")]
    [ApiController]
    public class PedidoControlador : ControllerBase
    {
        
        private readonly ILogger<PedidoControlador> _logger;

        private static int _id;

        private readonly IObterFormaPagamento _pagamento;

        public PedidoControlador(ILogger<PedidoControlador> logger, IObterFormaPagamento pagamento)
        {
            _logger = logger;
            _pagamento = pagamento;
        }

       
        [HttpPost]
        public  Task<ActionResult<Pedido>> PostTodoItem(Pedido pedido)
        {
           
            _logger.LogInformation("Acho A forma " +
                                   _pagamento.FormaPagamento(FormaPagamento.ComDescricao("Cartao")));
            
            _id += 1;
            
            return Task.FromResult<ActionResult<Pedido>>(Ok("Salvo " + _id));

        }
    }
}