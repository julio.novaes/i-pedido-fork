using Domain.FormaPagamentos;

namespace WebApplication
{
    public class ObterFromaPagamento:IObterFormaPagamento
    {
        private IClienteRest clienteRest;

        public ObterFromaPagamento(IClienteRest clienteRest)
        {
            this.clienteRest = clienteRest;
        }

        public bool FormaPagamento(FormaPagamento pagamento)
        {
            var formaPagamentoExterna = clienteRest.obterFromaExterna(pagamento.Descricao).Result;
            return formaPagamentoExterna != null && pagamento.Equals(formaPagamentoExterna.paraForma());
        }
    }
}