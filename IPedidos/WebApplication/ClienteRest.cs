#nullable enable
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace WebApplication
{
    public class ClienteRest : IClienteRest
    {
        private readonly HttpClient _client;
        private IConfiguration _configuration;

        public ClienteRest(HttpClient client, IConfiguration configuration)
        {
            _client = client;
            _configuration = configuration;
        }
        
        private Task<Stream> ProcessRepositories(Uri uri)
        {
            
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            Task<Stream> processRepositories =  Task.FromResult(_client.GetStreamAsync(uri).Result);
            
            return  processRepositories;
        }
        public async Task<FormaPagamentoExterna?> obterFromaExterna(String forma)
        {
            var urlFroma = _configuration["UrLFroma"];
            
            return await JsonSerializer.DeserializeAsync<FormaPagamentoExterna>( ProcessRepositories(new Uri(urlFroma + forma)).Result);
        }

        
    }
}