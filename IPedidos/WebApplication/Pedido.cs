using System.Collections.Immutable;

namespace WebApplication
{
    public class Pedido
    {
        public string _cliente { get; set; }
        public string _vendedor { get; set; }
        public string _formaPagamento { get; set; }
        public ImmutableList<string> _produtos { get; set; }

    }
}