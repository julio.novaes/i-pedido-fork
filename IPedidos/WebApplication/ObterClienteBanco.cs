using System.Linq;
using Domain.Clientes;

namespace WebApplication
{
    public class ObterClienteBanco : IObterCliente
    {
        private readonly PedidoContext _context;

        public ObterClienteBanco(PedidoContext context)
        {
            _context = context;
        }

        public Cliente ObterCliente(Cliente cliente)
        {
            return _context.Clientes
                .FirstOrDefault(externo => externo
                    .NumeroContribuinte
                    .Equals(cliente.NumeroContribuinte)
                )?
                .ParaCliente();
        }
    }
}