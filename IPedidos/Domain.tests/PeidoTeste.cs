using System.Linq;
using Domain.Clientes;
using Domain.FormaPagamentos;
using Domain.Pedidos;
using Domain.Produtos;
using Domain.Vendendors;
using Xunit;

namespace Domain.tests
{
    public class PeidoTeste
    {
        [Fact]
        public void PedidoValioSalvar()
        {

            var salvar = new MockPedido();
            
            var pedido = Pedido
                .CreateInstance()
                .ComCliente
                (
                    Cliente
                        .Mock()
                )
                .ComProdutos(Produto.Mock())
                .ComVendedor(Vendedor.ComNome("Um Vendedor da Silva"))
                .ComFormaPagamento(FormaPagamento.ComDescricao("Um pagamento"))
                .Criar();
            
            pedido.Salvar(salvar);

            Assert.True( salvar.get().Contains(pedido));
        }
    }
}