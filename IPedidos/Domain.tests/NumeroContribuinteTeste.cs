
using Domain.Clientes;
using Xunit;

namespace Domain.tests
{
    public class NumeroContribuinteTeste
    {

        private NumeroContribuinte _numeroContribuinte = new PredicadoCnpj();

        [Fact]
        public void CenarioCpFinValid()
        {
            
            Assert.True(_numeroContribuinte.Test("85.446.604/0001-19"));
            
        }

        [Fact]
        void CenarioCadeiaComSegundoDigitoVerificadorErrado()
        {
            
            Assert.False(_numeroContribuinte.Test("85.446.604/0001-17"));
        }

        [Fact]
        public void CenarioCadeiaComPrimeiroDigitoVerificadorErrado() {

            Assert.False(_numeroContribuinte.Test("82.860.638/0001-47"));

        }   
        [Fact]
        void ContribuinteTipoCpfInvalidos() {

             Assert.False( _numeroContribuinte.Test(null));
             Assert.False( _numeroContribuinte.Test(""));
             Assert.False( _numeroContribuinte.Test("09877654432109"));
             Assert.False( _numeroContribuinte.Test("00.000.000/0000-00"));
             Assert.False( _numeroContribuinte.Test("11.111.111/1111-11"));
             Assert.False( _numeroContribuinte.Test("22.222.222/2222-22"));
             Assert.False( _numeroContribuinte.Test("33.333.333/3333-33"));
             Assert.False( _numeroContribuinte.Test("44.444.444/4444-44"));
             Assert.False( _numeroContribuinte.Test("55555555555555"));
             Assert.False( _numeroContribuinte.Test("66.666.666/6666-66"));
             Assert.False( _numeroContribuinte.Test("77.777.777/7777-77"));
             Assert.False( _numeroContribuinte.Test("88.888.888/8888-88"));
             Assert.False( _numeroContribuinte.Test("99.999.999/9999-99"));

        }
    }
    
  
}