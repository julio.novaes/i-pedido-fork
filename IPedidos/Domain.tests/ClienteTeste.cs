using Domain.Clientes;
using Xunit;

namespace Domain.tests
{
    public class ClienteTeste
    {
        [Fact]
        public void CriandoClienteValido()
        {
            var cliente = Cliente
                .Iniciar()
                .ComCidade("uma cidade")
                .ComNome("Fulano")
                .ComNumeroContribuinte("85.446.604/0001-19")
                .Criar();

            Assert.Equal("85.446.604/0001-19",cliente.NumeroContribuinte);
        }

        [Fact(DisplayName = "Cliente com numero de contribuinte invalido")]
        public void CriandoClienteInvalido()
        { 
            
            Assert.Throws<ClienteInvalido>(()=>Cliente
                .Iniciar()
                .ComCidade("uma cidade")
                .ComNome("Fulano")
                .ComNumeroContribuinte("85.446.604/0001-09")
                .Criar());

           
        }

        [Fact(DisplayName = "Cenario quando buscar um clinte com numero de contribuinte valido retornar")]
        public void BuscarClinete()
        {
            IObterCliente c = new MockObter();
            
            var cliente = Cliente
                .Iniciar()
                .ComNumeroContribuinte("85.446.604/0001-19")
                .Criar();

            var buscarCliente = cliente.BuscarCliente(c);
            
            Assert.Equal(cliente.NumeroContribuinte,buscarCliente.NumeroContribuinte);
        }
    }
}