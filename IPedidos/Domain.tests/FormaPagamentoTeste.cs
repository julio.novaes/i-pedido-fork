using Domain.FormaPagamentos;
using Xunit;

namespace Domain.tests
{
    public class FormaPagamentoTeste
    {
        [Fact(DisplayName = "Cenario forma de pagamento retorna valido")]
        public void TesteCriarFormaPagamento()
        {
            var f = FormaPagamento.ComDescricao("A Vista");
            
            Assert.Equal(f,f.FormadePagamentoValida( new MockFormaPagamento()));
            
        }
        
        
        [Fact(DisplayName = "Cenario forma de pagamento retorna INVALIDO")]
        public void TesteCriarFormaPagamentoInvalida()
        {
            var f = FormaPagamento.ComDescricao("A Vista");
            
            Assert.Throws<FormaPagamentoInvalido>(()=> f.FormadePagamentoValida( new ObterFormaInvlida()));
            
        }
        [Fact(DisplayName = "Cenario forma de pagamento equal null")]
        public void TesteEqualFormaPagamentonull()
        {
            var f = FormaPagamento.ComDescricao("A Vista");
            
            Assert.False(f.Equals(null));
            
        }
        
        [Fact(DisplayName = "Cenario forma de pagamento equal obj e obj")]
        public void TesteEqualFormaPagamentonobjeobj()
        {
            var f = FormaPagamento.ComDescricao("A Vista");
            
            Assert.True(f.Equals(f));
            
        }
        
    }
    
} 