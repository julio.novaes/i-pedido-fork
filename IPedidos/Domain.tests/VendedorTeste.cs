using Domain.Vendendors;
using Xunit;

namespace Domain.tests
{
    public class VendedorTeste
    {
        [Fact]
        public void CriandoVendedorValido()
        {
            var vendedor = Vendedor.ComNome("Fera");

            Assert.Equal("Fera", vendedor.ToString());
        }


        [Fact]
        public void CriandoVendedorIvlido()
        {
            var vendedor = Vendedor.ComNome("Fera");

            Assert.Throws<VendedorNaoENcontrado>(() => vendedor.Valido(new MockVendedorInvalido()));
        }

        [Fact]
        public void CriandoVendedorValidoEretornarVendendor()
        {
            var vendedor = Vendedor.ComNome("Fera");

            var expected = vendedor.Valido(new MockVendedor());

            Assert.Equal(vendedor, expected);
        }
        
        
        
        [Fact]
        public void VendedorEqualNull()
        {
            var vendedor = Vendedor.ComNome("Fera");

            Assert.False(vendedor.Equals(null));
        }
        
        [Fact]
        public void VendedorEqualObj()
        {
            var vendedor = Vendedor.ComNome("Fera");

            Assert.True(vendedor.Equals(vendedor));
        }
    }
    
}