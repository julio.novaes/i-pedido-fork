using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Domain.Produtos;
using Xunit;

namespace Domain.tests
{
    public class ProdutoTeste
    {
        [Fact]
        public void TesteCriarProduto()
        {
            Produto produto = Produto.Criar().ComDescricao("Caderno")
                .ComUnidade("Und")
                .ComPreco(10.00)
                .Finalizar();

            Assert.Equal("Caderno", produto.Descricao);
        }

        [Fact]
        public void QuandoRecuperarProdutosRetornar()
        {
            Produto produto = Produto
                .Criar()
                .ComDescricao("Carro")
                .Finalizar();

            List<string> list = new List<string>();

            list.Add("Carro");

            var immutableList = Produto.GetAll(list.ToImmutableList(), new MockObterProduto());

            var descricao = immutableList.First(_ => produto.Descricao == "Carro").Descricao;

            Assert.Equal("Carro", descricao);
        }

        [Fact]
        public void QuandoRecuperarProdutosExcessaoProdutoNaoEncontrado()
        {
            var list = new List<string> { "Carro" };

            Assert.Throws<ProdutoNaoEncontado>(() => Produto.GetAll(list.ToImmutableList(), new MockObterProdutoNaoEncontrado()));
        }


        [Fact]
        public void QuandoRecuperarProdutosExcessao()
        {
            var list = new List<string> { "Carro" };

            Assert.Throws<ProdutoNaoEncontado>(() => Produto.GetAll(list.ToImmutableList(), new MockObterProdutoComDescriacaoInvalida()));
        }
        
    }
}