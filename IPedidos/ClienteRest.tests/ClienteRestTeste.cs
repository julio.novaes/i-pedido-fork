using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Domain.FormaPagamentos;
using Domain.Produtos;
using Moq;
using Moq.Protected;
using Xunit;

namespace ClienteRest.tests
{
    public class ClienteRestTeste
    {
        private readonly Mock<HttpMessageHandler> _mockHttp;
        private HttpClient httpClient;


        public ClienteRestTeste()
        {
            _mockHttp = _mockHttp = new Mock<HttpMessageHandler>();

            httpClient = new HttpClient(_mockHttp.Object);
        }


        [Fact]
        public  void QunadoBUscarUmPRodutoNawebRetornar()
        {
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(
                    @"{""Descrição"":""Feijao"",""Unidade"":""Quilograma"",""preço"":""15.00""}"),
            };

            const string sourceUrl = "http://localhost:3001/produto/Feijao";

            NewMethod(response);
            
            var cliente = new ClienteRest(httpClient);

            var webproduto = cliente.Webproduto(new Uri(sourceUrl));

            Assert.Equal("Feijao", webproduto.Result?.Descricao);
        }
        
        [Fact]
        public  void QunadoBUscarUmFormaDePagamentoNawebRetornar()
        {
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(
                    @"{""Forma de Pagamento"":""Cartao""}"),
            };

            const string sourceUrl = "http://localhost:3001/produto/Feijao";

            NewMethod(response);
            
            var cliente = new ClienteRest(httpClient);

            var webproduto = cliente.Webforma(new Uri(sourceUrl));

            Assert.Equal("Cartao", webproduto.Result?.FormadeDescircao);
        }

        
         
        [Fact]
        public void QuandoRecuperarProdutosRetornarWeb()
        {
            
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(
                    @"{""Descrição"":""Carro"",""Unidade"":""Quilograma"",""preço"":""15.00""}"),
            };

            var sourceUrl = new Uri("http://localhost:3001/produto/Carro");

            NewMethod(response);
            
            var list = new List<string>();

            list.Add("Carro");

            var ob = new Fabrica(httpClient).ObterP(sourceUrl);
            
            var immutableList = Produto.GetAll(list.ToImmutableList(),ob);

            var descricao = immutableList.First(produto =>   produto.Descricao == "Carro").Descricao;

            Assert.Equal("Carro", descricao);
        }
        
        
        [Fact(DisplayName = "Cenario forma de pagamento retorna valido da web")]
        public void TesteCriarFormaPagamentoWeb()
        {
            
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(
                    @"{""Forma de Pagamento"":""Cartao""}"),
            };

            var sourceUrl = new Uri("http://localhost:3001/formapagamento/Cartao");

            NewMethod(response);

            var ofp = new Fabrica(httpClient).ObterFP(sourceUrl);
            
            var f = FormaPagamento.ComDescricao("Cartao");
            
            Assert.Equal(f,f.FormadePagamentoValida(ofp));
            
        }
        
        private void NewMethod(HttpResponseMessage response)
        {
            _mockHttp
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(response)
                .Verifiable();
        }
    }
}