using System.Collections.Generic;
using System.Collections.Immutable;
using Domain.Clientes;
using Domain.FormaPagamentos;
using Domain.Pedidos;
using Domain.Produtos;
using Domain.Vendendors;
using Xunit;

namespace Aplicacao.tests
{
    public class RelizarPedidoTeste
    {
        
        [Fact]
        public void PedidoComClienteInvalido()
        {

            IRealizarPedido pedido = new RelizarPedido(new MockObter(), 
                new MockObterProduto(),
                new MockVendedor(),
                new MockFormaPagamento(),
                null
                );
            
           Assert.Throws<ClienteInvalido>(()=> 
               pedido
               .EfetuarPedido(
                   "85.446.604/0001-09",
                   "A Vista",
                   "Fulano",
                   new List<string>().ToImmutableList()
                   )
           );
        }
        
        [Fact]
        public void PedidoComProdutoInvalido()
        {

            IRealizarPedido pedido = new RelizarPedido(new MockObter(), 
                new MockObterProdutoComDescriacaoInvalida(),
                new MockVendedor(),
                new MockFormaPagamento(),
                null
            );
            
            Assert.Throws<ProdutoNaoEncontado>(()=>
            {
                var list = new List<string> { "Um produto" };
                pedido
                    .EfetuarPedido(
                        "85.446.604/0001-19",
                        "A Vista",
                        "Fulano",
                        list.ToImmutableList()
                    );
            });
        }
        
        [Fact]
        public void PedidoComVendendorInvalido()
        {

            IRealizarPedido pedido = new RelizarPedido(new MockObter(), 
                new MockObterProduto(),
                new MockVendedorInvalido(),
                new MockFormaPagamento(),
                null
            );
            
            Assert.Throws<VendedorNaoENcontrado>(()=>
            {
                var produtos = new List<string>();
                produtos.Add("Alguma coisa");
                
                pedido
                    .EfetuarPedido(
                        "85.446.604/0001-19",
                        "A Vista",
                        "Fulano",
                        produtos.ToImmutableList()
                    );
            });
        }
        
        
        [Fact]
        public void PedidoComFormaPagamentoInvalido()
        {

            IRealizarPedido pedido = new RelizarPedido(new MockObter(), 
                new MockObterProduto(),
                new MockVendedor(),
                new MockFormaPagamento(),
                null
            );
            
            Assert.Throws<FormaPagamentoInvalido>(()=>
            {
                var produtos = new List<string>();
                produtos.Add("carro");
                pedido
                    .EfetuarPedido(
                        "85.446.604/0001-19",
                        "PIX",
                        "Fulano",
                        produtos.ToImmutableList()
                    );
            });
            
            
        }
        
        [Fact]
        public void SalvarPedido()
        {
            var salvarPedido = new MockPedido();
            IRealizarPedido pedido = new RelizarPedido(new MockObter(), 
                new MockObterProduto(),
                new MockVendedor(),
                new MockFormaPagamento(),
                salvarPedido
            );

            var produtos = new List<string> { "Carro" };

            pedido
                    .EfetuarPedido(
                        "85.446.604/0001-19",
                        "A Vista",
                        "Fulano",
                        produtos.ToImmutableList()
                    );
            
            Assert.True(salvarPedido.get().Count != 0);

        }
    }

}