using System.Collections.Generic;
using System.Collections.Immutable;
using Domain.Clientes;
using Domain.FormaPagamentos;
using Domain.Pedidos;
using Domain.Produtos;
using Domain.Vendendors;

namespace Aplicacao
{
    public sealed class RelizarPedido : IRealizarPedido
    {
        private IObterCliente _obterCliente;
        private IObterProduto _obterProduto;
        private IObterFormaPagamento _pagamento;
        private IObterVendedor _obterVendedor;
        private ISalvarPedido _salvarPedido;


        public RelizarPedido(IObterCliente obterCliente, IObterProduto obterProduto, IObterVendedor obterVendedor,
            IObterFormaPagamento pagamento, ISalvarPedido salvarPedido)
        {
            _obterCliente = obterCliente;
            _obterProduto = obterProduto;
            _obterVendedor = obterVendedor;
            _pagamento = pagamento;
            _salvarPedido = salvarPedido;
        }

        public void EfetuarPedido(string numeroContribuinte, string descricaoFormaPagamento, string vendedor,
            IImmutableList<string> produtos)
        {
            Pedido
                .CreateInstance()
                .ComCliente(cliente: Cliente
                    .Iniciar()
                    .ComNumeroContribuinte(numeroContribuinte)
                    .Criar().BuscarCliente(_obterCliente))
                .ComProdutos(Produto.GetAll(produtos, _obterProduto))
                .ComFormaPagamento(FormaPagamento
                    .ComDescricao(descricaoFormaPagamento)
                    .FormadePagamentoValida(_pagamento))
                .ComVendedor(Vendedor.ComNome(vendedor).Valido(_obterVendedor))
                .Criar()
                .Salvar(_salvarPedido);
        }
    }
}