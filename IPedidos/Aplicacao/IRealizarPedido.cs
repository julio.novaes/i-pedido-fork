﻿using System.Collections.Immutable;

namespace Aplicacao
{
    public interface IRealizarPedido
    {
        void EfetuarPedido
        (
            string numeroContribuinte,
            string descricaoFormaPagamento,
            string vendedor,
            IImmutableList<string> produtos
        );
    }
}