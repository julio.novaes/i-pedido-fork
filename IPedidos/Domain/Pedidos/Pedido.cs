using System;
using System.Collections.Immutable;
using Domain.Clientes;
using Domain.FormaPagamentos;
using Domain.Produtos;
using Domain.Vendendors;

namespace Domain.Pedidos
{
    public sealed class Pedido
    {
        private readonly Cliente _cliente;
        private readonly Vendedor _vendedor;
        private readonly FormaPagamento _formaPagamento;
        private readonly IImmutableList<Produto> _produtos;

        private Pedido(PedidoBulid bulid)
        {
            _cliente = bulid.Cliente;
            _vendedor = bulid.Vendedor;
            _formaPagamento = bulid.FormaPagamento;
            _produtos = bulid.Produtos;
        }

        public static PedidoBulid CreateInstance()
        {
            return new PedidoBulid();
        }

        public void Salvar(ISalvarPedido pedido)
        {
            pedido.Salvar(this);
        }

        private bool Equals(Pedido other)
        {
            return Equals(_cliente, other._cliente)
                   && Equals(_vendedor, other._vendedor) 
                   && Equals(_formaPagamento, other._formaPagamento) 
                   && Equals(_produtos, other._produtos);
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is Pedido other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_cliente, _vendedor, _formaPagamento, _produtos);
        }

        public sealed class PedidoBulid
        {
            internal Cliente Cliente { get; private set; }

             internal Vendedor Vendedor { get; private set; }
             internal FormaPagamento FormaPagamento { get; private set; }

             internal IImmutableList<Produto> Produtos { get; private set; }

           

            public PedidoBulid ComCliente(Cliente cliente)
            {
                Cliente = cliente;
                return this;
            }

            public PedidoBulid ComFormaPagamento(FormaPagamento formaPagamento)
            {
                FormaPagamento = formaPagamento;
                return this;
            }

            public PedidoBulid ComVendedor(Vendedor vendedor)
            {
                Vendedor = vendedor;
                return this;
            }

            public PedidoBulid ComProdutos(IImmutableList<Produto> produtos)
            {
                Produtos = produtos;
                return this;
            }

            public Pedido Criar()
            {
                return new Pedido(this);
            }
        }
    }
}