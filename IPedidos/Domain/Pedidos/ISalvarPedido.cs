using System.Collections.Generic;
using Domain.Produtos;

namespace Domain.Pedidos
{
    public interface ISalvarPedido
    {
        void Salvar(Pedido pedido);
    }

    public class MockPedido : ISalvarPedido
    {
        private ISet<Pedido> _pedidos = new SortedSet<Pedido>(); 
        public void Salvar(Pedido pedido)
        {
            _pedidos.Add(pedido);
        }

        public ISet<Pedido> get()
        {
            return _pedidos;
        }
        
    }
}