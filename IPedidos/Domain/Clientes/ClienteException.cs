using System;

namespace Domain.Clientes
{
    public class ClienteException : Exception
    {
        private readonly string _numeroContribuinte;
        private readonly CodigoErro _codigoErro;

        public ClienteException(string numeroContribuinte, CodigoErro codigoErro)
        {
            _numeroContribuinte = numeroContribuinte;
            _codigoErro = codigoErro;
        }
    }

    public class ClienteInvalido : ClienteException 
    {
        public ClienteInvalido(string numeroContribuinte) 
            : base(numeroContribuinte, CodigoErro.InvalidoEncontrado)
        {
        }
    }

    public enum CodigoErro
    { 
        ClienteNaoEncontrado = 2548406,
        InvalidoEncontrado = 2548500
    }
}