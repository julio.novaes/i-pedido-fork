namespace Domain.Clientes
{
    public sealed class Cliente
    {
        internal Cliente(ClienteBuild clienteBuild)
        {
            Nome = clienteBuild.Nome;
            NumeroContribuinte = clienteBuild.NumeroContribuinte;
            Cidade = clienteBuild.Cidade;
        }

        public readonly string NumeroContribuinte;

        public readonly string Nome;

        public readonly string Cidade;

        public static ClienteBuild Iniciar()
        {
            return new ClienteBuild();
        }
        
        public static Cliente Mock()
        {
            return new ClienteBuild()
                .ComCidade("Irecê")
                .ComNome("Fulano")
                .ComNumeroContribuinte("59.615.849/0001-12")
                .Criar();
        }

        public Cliente BuscarCliente(IObterCliente cliente)
        {
            return cliente.ObterCliente(this);
        }
    }

    public class ClienteBuild
    {
        protected internal string NumeroContribuinte { get; private set; }

        protected internal string Nome { get; private set; }

        protected internal string Cidade { get; private set; }

        public ClienteBuild()
        {
        }

        public ClienteBuild(Cliente build)
        {
            NumeroContribuinte = build.NumeroContribuinte;
            Nome = build.Nome;
            Cidade = build.Cidade;
        }

        public ClienteBuild ComNome(string nome)
        {
            Nome = nome;
            return this;
        }

        public ClienteBuild ComNumeroContribuinte(string numeroContribuinte)
        {
            if (! new PredicadoCnpj().Test(numeroContribuinte)) throw new ClienteInvalido(numeroContribuinte);
            NumeroContribuinte = numeroContribuinte;
            return this;
        }

        public ClienteBuild ComCidade(string cidade)
        {
            Cidade = cidade;
            return this;
        }

        public Cliente Criar()
        {
            return new Cliente(this);
        }
    }
}