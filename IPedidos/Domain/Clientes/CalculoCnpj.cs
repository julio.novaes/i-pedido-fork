using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Clientes
{
    public sealed class CalculoCnpj
    {
        private static int MODULO_NUMERO_CONTRIBUINTE = 11;

        internal static int[] TransformarCadeiaDeCaracteresEmArray(string cadeia)
        {
            var replace = cadeia
                .Replace(".", "")
                .Replace("-", "")
                .Replace("/", "")
                .Replace(" ", "");


            var charArray = replace.ToCharArray();

            return charArray.Select(c => Convert.ToInt32(Char.GetNumericValue(c))).ToArray();
        }

        public static int CalcularDigitoVerificador(int[] cadeiaNumerica, int basePrimerioDigito, int tamanhoCadeia)
        {
            var resultado = SomarPosicoesDeCadeiaNumerica(cadeiaNumerica, basePrimerioDigito, tamanhoCadeia);

            return CalcularDigitoPorRestricaoDeBase(resultado);
        }


        private static int SomarPosicoesDeCadeiaNumerica(IReadOnlyList<int> cadeiaNumerica, int baseN, int limite)
        {
            var somatoria = 0;

            for (int incide = limite - 1, fatorIncremental = 0; incide >= 0; incide--, fatorIncremental++)
            {
                int fatorMultiplicador = ProximoFatorMultiplicador(baseN, fatorIncremental);

                somatoria += fatorMultiplicador * cadeiaNumerica[incide];
            }

            return somatoria;
        }


        private static int ProximoFatorMultiplicador(int baseN, int fatorIncremental)
        {
            return 2 + (fatorIncremental % baseN);
        }


        private static int CalcularDigitoPorRestricaoDeBase(int resultado)
        {
            var resto = resultado % MODULO_NUMERO_CONTRIBUINTE;

            if (resto < 2) return 0;

            return MODULO_NUMERO_CONTRIBUINTE - resto;
        }

        public static bool DigitoVerificadorIncorreto(int indiceDigito, int[] cadeiaNumerica,
            int possivelDigitoVerificador)
        {
            return possivelDigitoVerificador != cadeiaNumerica[indiceDigito - 1];
        }
    }
}