namespace Domain.Clientes
{
    public class MockObter:IObterCliente
        {
            public Cliente ObterCliente(Cliente cliente)
            {
                return new ClienteBuild(cliente)
                    .ComCidade("Uma cidade")
                    .ComNome("Fulano sa")
                    .Criar();

            }
        }
    }
