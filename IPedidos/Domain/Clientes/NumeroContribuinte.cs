
using System.Text.RegularExpressions;

namespace Domain.Clientes
{
    public abstract class NumeroContribuinte
    {
        private readonly int _tamanhoCadeia;
        private readonly int _basePrimerioDigito;
        private readonly int _baseSegundoDigito;
        protected NumeroContribuinte(int tamanhoCadeia, int basePrimeiroDigito, int baseSegundoDigito)
        {
            _tamanhoCadeia = tamanhoCadeia;
            _basePrimerioDigito = basePrimeiroDigito;
            _baseSegundoDigito = baseSegundoDigito;
        }


        public bool Test(string cadeiaNumeroContribuinte)
        {
            if (cadeiaNumeroContribuinte == null) return false;

            if (!MatchExpressaoRegular(cadeiaNumeroContribuinte)) return false;
            
            var numeroContribuinte = CalculoCnpj.TransformarCadeiaDeCaracteresEmArray(cadeiaNumeroContribuinte);

            return !DigitosIguais(numeroContribuinte) && DigitosVerificadoresValidos(numeroContribuinte);
        }

        private bool DigitosVerificadoresValidos(int[] numeroContribuinte)
        {
            var primeiroDigito =
                CalculoCnpj
                    .CalcularDigitoVerificador(numeroContribuinte, _basePrimerioDigito, _tamanhoCadeia - 2);
                
            if (PrimeiroDigitoVerificadorIncorreto(numeroContribuinte, primeiroDigito)) return false;

             var segundoDigito = CalculoCnpj
                 .CalcularDigitoVerificador(numeroContribuinte, _baseSegundoDigito, _tamanhoCadeia - 1);
             
            
            
             return SegundoDigitoVerificadorCorreto(numeroContribuinte, segundoDigito);

        }

        private bool DigitosIguais(int[] digitos)
        {
            var i = 1;
            
            while (i < _tamanhoCadeia)
            {
                if (digitos[0] != digitos[i++]) return false;
            }

            return true;
        }


        private bool PrimeiroDigitoVerificadorIncorreto(int[] numeroContribuinte, int digitoVerificador)
        {
            var indice = IndiceDoPrimeiroDigitoVerificador();
            return CalculoCnpj.DigitoVerificadorIncorreto(indice, numeroContribuinte, digitoVerificador);
            
        }

        private bool SegundoDigitoVerificadorCorreto(int[] numeroContribuinte, int digitoVerificador)
        {
            var indice = IndiceDoSegundoDigitoVerificador();

            return !CalculoCnpj.DigitoVerificadorIncorreto(indice, numeroContribuinte, digitoVerificador);


        }

        protected abstract bool MatchExpressaoRegular(string numeroContribuinte);

        protected abstract int IndiceDoPrimeiroDigitoVerificador();


        protected abstract int IndiceDoSegundoDigitoVerificador();
    }

    public class PredicadoCnpj : NumeroContribuinte
    {

        public PredicadoCnpj():base( 14,8,8)
        {
            
        }

        private static readonly Regex ExpressaoRegularCnpj = new("^\\d{2}\\.?\\d{3}\\.?\\d{3}\\/?\\d{4}-?\\d{2}$");


        protected override bool MatchExpressaoRegular(string numeroContribuinte)
        {
            return ExpressaoRegularCnpj.Match(numeroContribuinte).Success;
        }

        protected override int IndiceDoPrimeiroDigitoVerificador()
        {
            return 13;
        }

        protected override int IndiceDoSegundoDigitoVerificador()
        {
            return 14;
        }


      
    }
}