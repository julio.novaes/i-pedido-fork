namespace Domain.Clientes
{
    public interface IObterCliente
    {
        public Cliente ObterCliente(Cliente cliente);
    }
}