using System;

namespace Domain.Produtos
{
    public class ProdutoException:Exception
    {
        public string Codigo { get; }
        public CodigoErro Erro { get; }


        public ProdutoException(string codigo, CodigoErro codigoErro): base(codigo+" - "+codigoErro)
        {
            Codigo = codigo;
            Erro = codigoErro;
        }
    }
}