using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Domain.Produtos
{
    public sealed class Produto
    {
        public readonly string Descricao, Unidade;

        public readonly double Preco;

         internal Produto(ProdutoBuilder p) {
            Descricao = p.Descricao;
            Unidade = p.Unidade;
            Preco = p.Preco;
        }
         
        public static IImmutableList<Produto> GetAll(IImmutableList<string> codigos, IObterProduto produtos)
        {
            
            
            var immutableList = codigos
                .Select(s => produtos.Obter(s)).ToImmutableList();
            
            if (immutableList.IsEmpty)
            {
                throw new ProdutoNaoEncontado("Nenhum Produto encontrado", CodigoErro.ProdutoNaoEncontrado);
            }
            if (immutableList.Any(produto => produto == null))
            {
                throw new ProdutoNaoEncontado("Nenhum Produto encontrado", CodigoErro.ProdutoNaoEncontrado);
            }
            
            var except = immutableList
                .Select(produto => produto.Descricao)
                .ToImmutableList()
                .Except(codigos)
                .ToImmutableList().IsEmpty;
            
            if (!except) throw new ProdutoNaoEncontado(false.ToString(), CodigoErro.ProdutoNaoEncontrado);
            
            return immutableList;
            
        }

        public static IImmutableList<Produto> Mock()
        {
            List<Produto> list;
            list = new List<Produto>();


            var finalizar = Produto.Criar().ComDescricao("Caixa de produto")
                .ComPreco(10.00)
                .ComPreco(double.MaxValue)
                .ComUnidade(string.Empty)
                .Finalizar();
            
            list.Add(finalizar);
            list.Add(new ProdutoBuilder(finalizar)
                .ComDescricao("Feijao")
                .Finalizar());
            list.Add(new ProdutoBuilder(finalizar)
                .ComDescricao("Soja")
                .Finalizar());
            return list.ToImmutableList();
        }

        public static ProdutoBuilder Criar() {
            return new ProdutoBuilder();
        }

    }

    public class ProdutoBuilder {

        protected internal string Descricao {get; private set;} 
        protected internal string Unidade {get; private set;}
        protected internal double Preco {get; private set;}

        public ProdutoBuilder()
        {
        }
        public ProdutoBuilder(Produto produto)
        {

            Descricao = produto.Descricao;
            Unidade = produto.Unidade;
            Preco = produto.Preco;
        }

        

        public ProdutoBuilder ComDescricao(string descricao) {
            Descricao = descricao;
            return this;
        }

        public ProdutoBuilder ComUnidade(string unidade){
            Unidade = unidade;
            return this;
        }

        public ProdutoBuilder ComPreco(double preco) {
            Preco = preco;
            return this;
        }

        public Produto Finalizar() {
            return new Produto(this);
        }
    } 
}
