namespace Domain.Produtos
{
    public class MockObterProduto : IObterProduto
    {
        public Produto? Obter(string descricao)
        {
            return Produto
                .Criar()
                .ComDescricao(descricao)
                .ComPreco(100.0)
                .ComUnidade("un")
                .Finalizar();
        }
    }

    public class MockObterProdutoNaoEncontrado : IObterProduto
    {
        public Produto? Obter(string descricao)
        {
            return null;
        }
    }

    
    public class MockObterProdutoComDescriacaoInvalida : IObterProduto
    {
        public Produto? Obter(string descricao)
        {
            return Produto
                .Criar()
                .ComDescricao("Onubus")
                .ComPreco(double.MaxValue)
                .ComUnidade(string.Empty)
                .Finalizar();
        }
    }
}