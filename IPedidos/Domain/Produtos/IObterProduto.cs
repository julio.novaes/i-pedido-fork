namespace Domain.Produtos
{
    public interface IObterProduto
    {
        Produto Obter(string descricao);
    }
}