namespace Domain.Produtos
{
    public class ProdutoNaoEncontado :ProdutoException
    {
        public ProdutoNaoEncontado(string codigo, CodigoErro codigoErro) : base(codigo, codigoErro)
        {
        }
    }
}