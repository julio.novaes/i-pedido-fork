using System.Collections.Generic;
using System.Linq;

namespace Domain.FormaPagamentos
{
    public class MockFormaPagamento : IObterFormaPagamento
    {
        private List<FormaPagamento> _list = new();

        public MockFormaPagamento()
        {
            _list.Add(  FormaPagamentos.FormaPagamento.ComDescricao("A Vista"));
            _list.Add(  FormaPagamentos.FormaPagamento.ComDescricao("A Prazo"));
            _list.Add(  FormaPagamentos.FormaPagamento.ComDescricao("Cartao"));
        }

        public bool FormaPagamento( FormaPagamento descricao)
        {
            return _list.Any(pagamento => pagamento.Equals(descricao) );
        }
    }

     public class ObterFormaInvlida : IObterFormaPagamento
    {

        public bool FormaPagamento( FormaPagamento descricao)
        {
            return false;
        }
    }
}