namespace Domain.FormaPagamentos
{
    public interface IObterFormaPagamento
    {
        public bool FormaPagamento(FormaPagamento pagamento);
    }
}