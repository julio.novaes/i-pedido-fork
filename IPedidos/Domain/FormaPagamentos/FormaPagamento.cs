using System;

namespace Domain.FormaPagamentos
{
    public sealed class FormaPagamento
    {
        public string Descricao{ get;}

        private FormaPagamento(string descricao)
        {
            Descricao = descricao
                .Replace(" ","")
                .ToLower();
        }

        public static FormaPagamento ComDescricao(string descricao)
        {
            return new FormaPagamento(descricao);
        }


        public FormaPagamento FormadePagamentoValida(IObterFormaPagamento pagamento)
        {
           if (!pagamento.FormaPagamento(this)) throw new FormaPagamentoInvalido
               (
                   Descricao,CodigoErro.FROMAPAGRAMENTOINVLIDA
                   
                   );
            return this;
        }

        private bool Equals(FormaPagamento other)
        {
            return Descricao == other.Descricao;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((FormaPagamento)obj);
        }
        
    }

    public class FormaPagamentoInvalido : Exception
    {
        
        public FormaPagamentoInvalido(string codigo, CodigoErro codigoErro) 
            : base(codigo + " - " + codigoErro)
        {
           
        }
    }

    public enum CodigoErro
    {
        FROMAPAGRAMENTOINVLIDA = 2548501
    }
}