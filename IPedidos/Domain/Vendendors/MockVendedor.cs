namespace Domain.Vendendors
{
    public class MockVendedor : IObterVendedor
    {
        public Vendedor BuscarVendendor(string numeroInstitucional)
        {
            return Vendedor.ComNome(numeroInstitucional);
        }
    }

    public class MockVendedorInvalido : IObterVendedor
    {
        public Vendedor BuscarVendendor(string numeroInstitucional)
        {
            return Vendedor.ComNome("");
        }
    }
}