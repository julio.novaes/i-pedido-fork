using System;

namespace Domain.Vendendors
{
    public sealed class Vendedor
    {
        private Vendedor(string nome)
        {
            Nome = nome;
        }

        private string Nome { get; }

        public static Vendedor ComNome(string nome)
        {
            return new Vendedor(nome);
        }

        public Vendedor Valido(IObterVendedor iVendedor)
        {
            if (!Equals(iVendedor.BuscarVendendor(Nome))) throw new VendedorNaoENcontrado(Nome,CodigoErro.Vendendornaoencontrado);
            
            return this;
        }

        public override string ToString()
        {
            return Nome;
        }


        private bool Equals(Vendedor other)
        {
            return Nome == other.Nome;
        }

        public override bool Equals(object obj)
        {
            return !ReferenceEquals(null, obj);
        }

        public override int GetHashCode()
        {
            return Nome != null ? Nome.GetHashCode() : 0;
        }
    }

    
    public class VendedorNaoENcontrado : Exception
    {

        public VendedorNaoENcontrado(string numeroContribuinte, CodigoErro codigoErro): base(numeroContribuinte+" - "+ codigoErro)
        {
           
        }
    }

    public enum CodigoErro
    {
        Vendendornaoencontrado = 2548502
    }
}