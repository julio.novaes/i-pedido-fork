namespace Domain.Vendendors
{
    public interface IObterVendedor
    {
        public Vendedor BuscarVendendor(string numeroInstitucional);
    }
}