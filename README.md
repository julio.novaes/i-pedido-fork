# iPedido

![Diagrama de componentes](Diagrama/Component Diagram1.jpg)


##  API Para validar CPF e CNPJ  

![Diagrma_De_Atividades_CPF_CNPJ](Diagrama/CalculoCNPJ.jpg)


------

### Etapas para o desenvolvimento

* Domínio
- [x] Cliente
  - [x] Exceção
  - [x] Interface para obter
  - [x] Teste
- [x] Forma de pagamento
  - [x] Exceção
  - [x] Interface para obter
  - [x] Teste
- [x] Pedido
  - [X] Exceção
  - [x] Interface para salvar
  - [X] Teste
- [x] Produto
  - [x] Exceção
  - [x] Interface para obter
  - [X] Teste
- [X] Vendedor
  - [x] Exceção
  - [x] Interface para obter
  - [x] Teste

* Aplicação

- [X] Realizar Pedido
  - [X] Teste 

* Cliente Rest (Adaptador)

- [ ] Recuperar Cliente
  - [ ] Teste
- [X] Recuperar Forma de pagamento
  - [X] Teste
- [ ] Salvar pedido
  - [ ] Teste
- [X] Recuperar Produto
  - [X] Teste
- [ ] Recuperar Vendedor
  - [ ] Teste
  
* Web (Adaptador)

- [X] Realizar pedido
  - [ ] Teste

-------- 

# Etapas Fora o Código

- [ ] Diagramas de Sequências
- [X] Criar os mock server
- [ ] Criar DockerFile da aplicação
- [X] Criar Pipeline no Git lab
- [X] Incluir a forma de validar a busca por produtos
- [ ] Adicionar  no número de contribuinte o cpf para cliente
- [ ] Cliente rest quando retorna 404